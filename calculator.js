let calc = document.getElementById("Calc");
const answer = document.getElementById('Answer');
calc.addEventListener('click', (event) => {
    let firstArg = Number(document.getElementById("Arg1").value);
    let secondArg = Number(document.getElementById("Arg2").value);
    let operator = document.getElementById("Operator").value;

    let result;

    switch (operator) {
        case '+':
            result = firstArg + secondArg;
            break;

        case '-':
            result = firstArg - secondArg;
            break;

        case '*':
            result = firstArg * secondArg;
            break;

        case '/':
            result = firstArg / secondArg;
            break;

        case '%':
            result = firstArg % secondArg;
            break;

        default:
            console.error("Unknown operator")
    }
    answer.innerHTML = result;
});
